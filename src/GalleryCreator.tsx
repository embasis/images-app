import React from 'react';
import {Modal, Form, Alert, Button} from 'react-bootstrap';
import {catchError} from 'rxjs/operators';
import * as Api from './Api';

export interface GalleryCreatorProps {
    show: boolean,
    onHide: () => void,
    onGalleryCreate: () => void
}

const GalleryCreator: React.FC<GalleryCreatorProps> = ({show, onHide, onGalleryCreate}) => {
    const [galleryName, setGalleryName] = React.useState(''),
          [validated, setValidated] = React.useState(false),
          [showAlert, setShowAlert] = React.useState(false),
          [upload, setUpload] = React.useState(false);

    const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if (!e.currentTarget.checkValidity()) {
            e.stopPropagation();
        } else {
            setUpload(true);
        }

        setValidated(true);
    }

    React.useEffect(() => {
        if (upload) {
            const subscription = Api.createGallery(galleryName).pipe(
                catchError(() => 'error')
            ).subscribe(result => {
                if (result === 'success') {
                    onGalleryCreate();
                } else {
                    setUpload(false);
                    setShowAlert(true);
                }
            });
            return () => {
                subscription.unsubscribe();
            }
        }
    }, [upload]);

    return (
        <Modal show={show} onHide={onHide} className='black-color'>
            <Modal.Header closeButton>
                <Modal.Title>Add Gallery</Modal.Title>
            </Modal.Header>
            <Form noValidate validated={validated} onSubmit={onSubmit}>
                <Modal.Body>
                    <Alert variant='danger' dismissible show={showAlert} onClose={() => setShowAlert(false)}>
                        Error occured during gallery creation
                    </Alert>
                        <Form.Group>
                            <Form.Label>Gallery Name</Form.Label>
                            <Form.Control required value={galleryName} onChange={(e:any) => setGalleryName(e.target.value)}/>
                            <Form.Control.Feedback type='invalid'>
                                Cannot be empty
                            </Form.Control.Feedback>
                        </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button type='submit'>Create</Button>
                    <Button type='button' onClick={() => onHide()}>Close</Button>
                </Modal.Footer>
            </Form>
        </Modal>
    )
}

export default GalleryCreator;