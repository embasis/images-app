import {Observable, Subscriber, of} from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import {ajax, AjaxRequest} from 'rxjs/ajax';

export enum ResourceType {
    Gallery,
    Image
}

export enum SortCriteria {
    Name,
    CreationDate,
    ChangeDate
}

export interface SearchProps {
    order: SortCriteria,
    descending: boolean,
    galleryID?: string,
    searchPrefix: string,
    startCreatedAtDate?: Date,
    endCreatedAtDate?: Date,
    startLastChangedDate?: Date,
    endLastChangedDate?: Date
}

export interface ResourceId {
    id: string
}

export interface ResourceBase {
    name: string,
    createdAt: Date,
    lastChanged: Date,
}

export interface ResourceImage {
    galleryID: string,
    mimeType: string,
    width: number,
    height: number,
}

export interface ResourceGallery {
    imageCount: number
}

type ApiImage = ResourceBase & ResourceImage
type ApiGallery = ResourceBase & ResourceGallery
export type Image = ResourceId & ResourceBase & ResourceImage
export type Gallery = ResourceId & ResourceBase & ResourceGallery

const API_HOST = 'http://192.168.100.9:3001/';
const API_VERSION = 'v1';
const API_BASE = `${API_HOST}${API_VERSION}/`;
const API_BASE_AUTH = `${API_BASE}auth/`;

const url = (endpoint: string) => {
    return `${API_BASE}${endpoint}`;
}

const urlAuth = (endpoint: string) => {
    return `${API_BASE_AUTH}${endpoint}`;
}

const ajaxRequest = (endpoint: string, isAuth: boolean, body: any, method: string, mime?: string,
    progressCallback?: (percentComplete: number) => void) => {
    let req: AjaxRequest = {
        url: isAuth ? urlAuth(endpoint) : url(endpoint),
        method: method,
        withCredentials: true,
        body: body
    }
    if (mime) {
        req.headers = { 'Content-Type': mime };
    }
    if (progressCallback) {
        req.progressSubscriber = Subscriber.create((oEvent?) => {
            let num = 0;
            if (oEvent) {
                if (oEvent.lengthComputable) {
                    num = oEvent.loaded / oEvent.total * 100;
//                    console.log('so computable');
                } else {
                    num = 100;
//                    console.log('no computables :( sad af :(');
                }
            }
            progressCallback(num);
        });
    }
    return ajax(req);
}

const ajaxRequestJSON = (endpoint: string, isAuth: boolean, body: object, method: string) => {
    return ajaxRequest(endpoint, isAuth, body, method, 'application/json');
}

const ajaxRequestFormData = (endpoint: string, isAuth: boolean, form: FormData, method: string,
    progressCallback?: (percentComplete: number) => void) => {
    return ajaxRequest(endpoint, isAuth, form, method, undefined, progressCallback);
}

const ajaxRequestQuery = (endpoint: string, isAuth: boolean, queryParams: [string, string][], method: string) => {
    return ajaxRequest(`${endpoint}?${queryParams.map(([k,v]) => `${k}=${v}`).join('&')}`, isAuth, {}, method);
}

const resources = (endpoint: string, searchProps: SearchProps): Observable<string[]> => {
    return ajaxRequestJSON(endpoint, true, searchProps, 'POST').pipe(
        map(res => {
            if (res.response.result === 'success') {
                return res.response.resources;
            } else {
                throw res.response.result;
            }
        })
    )
}

const resource = (endpoint: string, id: string): Observable<Image | Gallery> => {
    return ajaxRequestQuery(endpoint, true, [['resourceID', id]], 'GET').pipe(
        map(res => {
            if (res.response.result === 'success') {
                return Object.assign({id: id}, res.response.resource,
                    {createdAt: new Date(res.response.resource.createdAt), lastChanged: new Date(res.response.resource.lastChanged)});
            } else {
                throw res.response.result;
            }
        })
    )
}

export const checkAuth = (): Observable<boolean> => {
    return ajaxRequest('checkauth', false, {}, 'POST').pipe(
        map(res => res.status === 200),
        catchError(e => of(false))
    );
}

export const signUp = (email: string, password: string): Observable<string> => {
    return ajaxRequestJSON('signup', false, {email: email, password: password}, 'POST').pipe(map(res => res.response.result));
}

export const login = (email: string, password: string): Observable<string> => {
    return ajaxRequestJSON('login', false, {email: email, password: password}, 'POST').pipe(map(res => res.response.result));
}

export const logout = (): Observable<string> => {
    return ajaxRequestJSON('logout', false, {}, 'POST').pipe(map(res => res.response.result));
}

export const galleries = (searchProps: SearchProps): Observable<string[]> => {
    return resources('galleries', searchProps);
}

export const images = (searchProps: SearchProps): Observable<string[]> => {
    return resources('images', searchProps);
}

export const gallery = (id: string): Observable<Gallery> => {
    return resource('gallery', id) as Observable<Gallery>;
}

export const image = (id: string): Observable<Image> => {
    return resource('image', id) as Observable<Image>;
}

export const uploadImage = (form: FormData, progressCallback?: (percentComplete: number) => void): Observable<string> => {
    return ajaxRequestFormData('uploadimage', true, form, 'PUT', progressCallback).pipe(map(res => res.response.result));
}

export const deleteImage = (id: string): Observable<string> => {
    return ajaxRequestJSON('deleteimage', true, {resourceID: id}, 'POST').pipe(map(res => res.response.result));
}

export const deleteGallery = (id: string): Observable<string> => {
    return ajaxRequestJSON('deletegallery', true, {resourceID: id}, 'POST').pipe(map(res => res.response.result));
}

export const createGallery = (name: string): Observable<string> => {
    return ajaxRequestJSON('creategallery', true, {name: name}, 'PUT').pipe(map(res => res.response.result));
}

export const makeImageSrc = (id: string, width: number, height: number): string => {
    return `${API_BASE_AUTH}file?resourceID=${id}&w=${width}&h=${height}`;
}