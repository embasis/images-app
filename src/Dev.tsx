import React from 'react';
import {concat, Observable} from 'rxjs';
import {map, catchError, toArray} from 'rxjs/operators';
import * as Api from './Api';

const requestLogin = () => {
    return Api.login('dolboeb@kek.com', '9019010oloLO');
}

const request = () => {
//    return Api.galleries({order: Api.SortCriteria.Name, descending: false});
//    return Api.deleteImage('5d478e1295bf92356b74b229');
//    return Api.checkAuth();
//    return Api.logout();
    return Api.gallery('5d4c41d0d4e8570ceb0b79ed');
}

const Dev: React.FC = (props) => {
    let [reqRes, setReqRes] = React.useState('');

    const req = requestLogin().pipe(
        catchError(err => {
            let str: string = err.status.toString();
            return str;
        })
    )
    const req2 = request().pipe(
        map(result => result.toString()),
//        catchError(err => {
//            let str: string = err.status.toString();
//            return str;
//        })
    )

    const req3 = (form: any) => {
        return Api.uploadImage(new FormData(form)).pipe(
            map(result => result.toString()),
            catchError(err => {
                let str: string = err.status.toString();
                return str;
            })
        )
    }
    
    return (
        <div>
            <p>Hello</p>
            <button type='button' onClick={e => req.subscribe(setReqRes)}>Send Login Request</button>
            <button type='button' onClick={e => req2.subscribe(setReqRes)}>Send Request</button>
            <form id='uploadform'>
                <input type='file' name='image' accept='image/png image/jpeg'></input>
                <input type='text' name='resourceID'></input>
                <button type="button" onClick={e => req3(document.getElementById('uploadform')).subscribe(setReqRes)}>Send</button>
            </form>
            <div id='reqresult'>
                {reqRes}
            </div>
        </div>
    );
} 

export default Dev;