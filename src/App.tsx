import React, { useEffect } from 'react';
import logo from './logo.svg';
import Dev from './Dev';
import AuthPage from './AuthPage';
import MainComponent from './Main';
import {Spinner} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import * as Api from './Api';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import './App.css';

const HasAuthComponent: React.FC = (props) => {
    const [hasAuth, setHasAuth] = React.useState<boolean | undefined>(undefined);

    useEffect(() => {
        if (hasAuth === undefined) {
            const subscription = Api.checkAuth().subscribe(setHasAuth);
            return () => {
                subscription.unsubscribe();
            }
        }
    }, [hasAuth]);

    if (hasAuth === undefined) {
        return (
            <Spinner animation='grow' />
        )
    } else if (hasAuth) {
        return (
            <MainComponent logout={() => setHasAuth(false)} />
        )
    } else {
        return (
            <AuthPage onSuccessfulAuth={() => setHasAuth(true)}/>
        )
    }
}

const App: React.FC = (props) => {
    return (
        <Router>
            <Route path="/" exact component={HasAuthComponent} />
            <Route path="/dev/" exact component={Dev} />
        </Router>
    );
}

export default App;