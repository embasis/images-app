import React from 'react';
import {Observable, of} from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import {Button, Container, Col, Row, Form, ButtonGroup, Alert} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import * as Api from './Api';

interface AuthFormProps {
    showConfirm: boolean,
    submitText: string,
    submitAction: (email: string, password: string) => Observable<string>,
    onSuccessfulSubmit: () => void
}

const AuthForm: React.FC<AuthFormProps> = (props) => {
    let passFeedbackFailedReqs = 'The password must be at least 6 characters long, have one small letter, big letter and digit',
        passFeedbackDoNotMatch = 'The passwords do not match!';

    let [validated, setValidated] = React.useState(false),
        [formValues, setFormValues] = React.useState({email: '', password: '', passwordConfirm: ''}),
        [passwordFeedbackText, setPasswordFeedbackText] = React.useState(''),
        [alertProps, setAlertProps] = React.useState<{alertText: string, type: 'success' | 'danger' | undefined, show: boolean}>
            ({alertText: '', type: undefined, show: false}),
        passRef: any = React.createRef(), passConfirmRef: any = React.createRef();

    let onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        const form = e.currentTarget;
        e.preventDefault();

        if (!form.checkValidity()) {
            e.stopPropagation();
        } else {
            props.submitAction(formValues.email, formValues.password).subscribe(msg => {
                if (msg === '') {
                    props.onSuccessfulSubmit();
                    if (props.showConfirm) {
                        setFormValues({email: '', password: '', passwordConfirm: ''});
                        setValidated(false);
                        setAlertProps({alertText: 'Success! Now check your email to complete the registration process!',
                            type: 'success', show: true});
                    }
                } else {
                    setAlertProps({alertText: msg, type: 'danger', show: true});
                }
            })
        }

        setValidated(true);
    }

    let onChange = (e: any) => {
        setFormValues(Object.assign({}, formValues, {[e.currentTarget.name]: e.target.value}));
    }

    React.useEffect(() => {
        if (props.showConfirm) {
            let check = formValues.password.length >= 6 && /[0-9]/.test(formValues.password) &&
                /[a-z]/.test(formValues.password) && /[A-Z]/.test(formValues.password)
            if (!check) {
                passRef.current.setCustomValidity('invalid');
                passConfirmRef.current && passConfirmRef.current.setCustomValidity('invalid');
                setPasswordFeedbackText(passFeedbackFailedReqs);
            } else if (formValues.password !== formValues.passwordConfirm) {
                passRef.current.setCustomValidity('invalid');
                passConfirmRef.current.setCustomValidity('invalid');
                setPasswordFeedbackText(passFeedbackDoNotMatch);
            } else {
                passRef.current.setCustomValidity('');
                passConfirmRef.current && passConfirmRef.current.setCustomValidity('');
            }
        } else {
            passRef.current.setCustomValidity('');
            setPasswordFeedbackText('');
        }
    }, [props.showConfirm, formValues.password, formValues.passwordConfirm,
    passRef, passConfirmRef, passFeedbackFailedReqs, passFeedbackDoNotMatch]);

    return (
        <div id='auth-form'>
            {alertProps.show &&
                <Alert variant={alertProps.type} onClose={() => setAlertProps(Object.assign({}, alertProps, {show: false}))} dismissible>
                    {alertProps.alertText}
                </Alert>
            }
            <Form noValidate validated={validated} onSubmit={onSubmit}>
                <Form.Group>
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control name='email' type='email' value={formValues.email} required onChange={onChange}></Form.Control>
                    <Form.Control.Feedback type='invalid'>
                        Invalid email address
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control name='password' type='password' value={formValues.password} required ref={passRef} onChange={onChange}></Form.Control>
                    <Form.Control.Feedback type='invalid'>
                        {passwordFeedbackText}
                    </Form.Control.Feedback>
                </Form.Group>
                {props.showConfirm &&
                    <Form.Group>
                        <Form.Label>Confirm Password</Form.Label>
                        <Form.Control name='passwordConfirm' type='password' value={formValues.passwordConfirm} required ref={passConfirmRef} onChange={onChange}></Form.Control>
                    </Form.Group>
                }
                <Button variant='primary' type='submit'>{props.submitText}</Button>
            </Form>
        </div>
    ) 
}

const AuthComponent: React.FC<{onSuccessfulAuth: () => void}> = (props) => {
    const signUpProps: AuthFormProps = {
        showConfirm: true,
        submitText: 'Sign Up',
        submitAction: (email, password) => {
            return Api.signUp(email, password).pipe(
                map(result => {
                    switch (result) {
                        case 'alreadyexists':
                            return 'User with such email address already exists';
                        case 'bademail':
                            return 'Email address is not a valid address';
                        case 'badpassword':
                            return 'Password doesn\'t satisfy the requirements';
                        case 'error':
                            return 'Unknown error. Try again';
                        case 'success':
                            return '';
                        default:
                            return '';
                    }
                }),
                catchError(err => of('Unknown error. Try again'))
            )
        },
        onSuccessfulSubmit: () => {}
    }
    const signInProps: AuthFormProps = {
        showConfirm: false,
        submitText: 'Sign In',
        submitAction: (email, password) => {
            return Api.login(email, password).pipe(
                map(result => {
                    switch (result) {
                        case 'badlogin':
                            return 'The provided user/password combination was not found on server. Try again';
                        case 'success':
                            return '';
                        default:
                            return '';
                    }
                }),
                catchError(err => of('Unknown error. Try again'))
            )
        },
        onSuccessfulSubmit: props.onSuccessfulAuth
    }

    let [curProps, setCurProps] = React.useState(signInProps);

    return (
        <div className='background-snd form-padding form-border'>
            <ButtonGroup>
                <Button onClick={(e: any) => setCurProps(signInProps)}>Sign In</Button>
                <Button onClick={(e: any) => setCurProps(signUpProps)}>Sign Up</Button>
            </ButtonGroup>
            <AuthForm {...curProps} />
        </div>
    )
}

export interface AuthPageProps {
    onSuccessfulAuth: () => void;
}

const AuthPage: React.FC<AuthPageProps> = (props) => {
    return (
        <Container>
            <Row id='auth-row'>
                <Col md={1}></Col>
                <Col md={5} id='auth-images'>
                    <h1>Images</h1>
                    <p>Save your images here</p>
                </Col>
                <Col md={5}>
                    <AuthComponent onSuccessfulAuth={props.onSuccessfulAuth}>
                    </AuthComponent>
                </Col>
            </Row>
        </Container>
    )
}

export default AuthPage;