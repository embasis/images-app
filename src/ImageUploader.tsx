import React from 'react';
import {catchError} from 'rxjs/operators';
import { Alert, Modal, Button, Form, Container, ProgressBar } from 'react-bootstrap';
import * as Api from './Api';

interface UploadResult {
    file: File,
    success: boolean,
    statusText: string
}

const UploadResults: React.FC<{results: UploadResult[]}> = (props) => {
    const resultToAlert = (result: UploadResult, index: number) => {
        return (
            <Alert key={index} variant={result.success ? 'success' : 'danger'}>
                <div className='overflow-ellipsis'>{result.file.name}</div>
                {result.statusText}
            </Alert>
        )
    }
    return (
        <div>
            {props.results.map(resultToAlert)}
        </div>
    )
}

interface ImageUploaderProps {
    show: boolean,
    onHide: () => void,
    onUpload: (value: boolean) => void,
    gallery?: Api.Gallery
}

interface UploadState {
    uploadList: File[],
    destGallery: Api.Gallery | null
}

const ImageUploader: React.FC<ImageUploaderProps> = ({show, onHide, onUpload, gallery}) => {
    const [uploadState, setUploadState] = React.useState<UploadState>({uploadList: [], destGallery: null}),
          [progress, setProgress] = React.useState<number>(0),
          [resultList, setResultList] = React.useState<[string, string][]>([]),
          inputRef: any = React.createRef();

    const fromFileList = (list: FileList) => {
        let result = [];
        for (let i = 0; i < list.length; ++i) {
            result.push(list[i]);
        }
        return result;
    }

    const alertFromFile = (file: File) => {
        return (
            <Alert variant='primary'>
                <div className='overflow-ellipsis'>{file.name}</div>
            </Alert>
        )
    }

    const alertFromResult = (fileName: string, result: string) => {
        return (
            <Alert variant={result === 'success' ? 'success' : 'danger'}>
                <div className='overflow-ellipsis'>{fileName}</div>
            </Alert>
        )
    }

    React.useEffect(() => {
        if (uploadState.destGallery) {
            const makeFormData = (file: File) => {
                let form = new FormData();
                
                form.append('resourceID', (uploadState.destGallery as Api.Gallery).id);
                form.append('image', file);

                return form;
            }

            if (uploadState.uploadList.length > 0) {
                let curUpload = uploadState.uploadList[0];

                const progressCallback = (percent: number) => {
                    console.log('progress: ', percent);
                    setProgress(percent);
                }
                let subscription = Api.uploadImage(makeFormData(curUpload), progressCallback).pipe(
                    catchError(err => 'error')
                ).subscribe(result => {
                    setProgress(0);
                    setResultList(resultList => {
                        let newResultList = resultList.slice();
                        newResultList.push([curUpload.name, result]);
                        return newResultList;
                    })
                    setUploadState(uploadState => {
                        let uploadList = uploadState.uploadList;
                        uploadList.splice(0, 1);

                        return Object.assign({}, uploadState, {uploadList: uploadList});
                    })
                })

                onUpload(true);

                return () => {
                    subscription.unsubscribe();
                }
            } else {
                onUpload(false);
            }
        }
    }, [uploadState, onUpload])

    return (
        <Modal show={show} onHide={onHide} className='black-color'>
            <Modal.Header closeButton>
                <Modal.Title>Upload Images</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {(uploadState === undefined || uploadState.uploadList.length === 0) && gallery !== undefined &&
                    <>
                        <p>Gallery: {gallery.name}</p>
                        <Button type='button' variant='primary' onClick={(e: any) => inputRef.current.click()}>Select Files</Button>
                    </>
                }
                {uploadState && uploadState.uploadList.length > 0 && gallery !== undefined &&
                    <>
                        <p>Uploading in gallery "{gallery.name}"</p>
                        <ProgressBar striped animated now={progress} />
                        <div className='filelist'>
                            {uploadState.uploadList.map(alertFromFile)}
                        </div>
                    </>
                }
                {resultList.length > 0 &&
                    <div className='filelist'>
                        {resultList.map(([fileName, result]) => alertFromResult(fileName, result))}
                    </div>
                }
                <Form>
                    <Form.Control ref={inputRef} type='file' multiple accept='image/jpeg,image/png' hidden
                        onChange={(e: any) => {
                            if (e.currentTarget.files && gallery) {
                                setProgress(0);
                                setUploadState({uploadList: fromFileList(e.currentTarget.files), destGallery: gallery});
                                onUpload(true);
                            } 
                        }}/>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant='primary' type='button' onClick={onHide}>Hide</Button>
            </Modal.Footer>
        </Modal>
    )
}

export default ImageUploader;