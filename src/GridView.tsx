import React from 'react';
import {fromEvent} from 'rxjs';
import * as Api from './Api';
import {Button, Navbar, Container, Image, Col, Row, Form, ButtonGroup, Alert, NavDropdown, Dropdown, Modal} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import logo from './logo.svg';
import moreImage from './more.png';
import { debounceTime } from 'rxjs/operators';
import { Table } from 'react-bootstrap';

class MoreToggle extends React.Component<{onClick: (e: any) => void}> {
    constructor(props: any, context: any) {
        super(props, context);
    }

    render() {
        return (
            <div className='more-button' onClick={(e: any) => {e.preventDefault(); this.props.onClick(e);}}>
                <Image src={moreImage} className='more-image'/>
            </div>
        )
    }
}

interface DetailsVisualProps {
    name: string,
    dateCreated: Date,
    dateChanged: Date,
    mimeType?: string,
    size?: string,
    imageCount?: number,
}

interface DetailsCoreProps {
    show: boolean,
    onHide: () => void
}

type DetailsProps = DetailsVisualProps & DetailsCoreProps

const DetailsModal: React.FC<DetailsProps> = (props) => {
    return (
        <Modal show={props.show} onHide={props.onHide} className='black-color'>
            <Modal.Header closeButton>
                <Modal.Title>Details</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Table>
                    <tr>
                        <td>Name</td>
                        <td>{props.name}</td>
                    </tr>
                    <tr>
                        <td>Created At</td>
                        <td>{props.dateCreated.toString()}</td>
                    </tr>
                    <tr>
                        <td>Last Changed At</td>
                        <td>{props.dateChanged.toString()}</td>
                    </tr>
                    {props.mimeType !== undefined &&
                    <tr>
                        <td>MIME Type</td>
                        <td>{props.mimeType}</td>
                    </tr>
                    }
                    {props.size !== undefined &&
                    <tr>
                        <td>Size</td>
                        <td>{props.size}</td>
                    </tr>
                    }
                    {props.imageCount !== undefined &&
                    <tr>
                        <td>Image Count</td>
                        <td>{props.imageCount}</td>
                    </tr>
                    }
                </Table>
            </Modal.Body>
            <Modal.Footer>
                <Button variant='primary' type='button' onClick={props.onHide}>Hide</Button>
            </Modal.Footer>
        </Modal>
    )
}

interface CellProps {
    image?: Api.Image,
    gallery?: Api.Gallery,
    showImage: (image?: Api.Image) => void,
    setGallery: (gallery: Api.Gallery) => void,
    showDetails: (props: DetailsVisualProps | null) => void,
    deleteImage: (id: string) => void,
    deleteGallery: (id: string) => void
}

const Cell: React.FC<CellProps> = (props) => {
    const [imgSrc, setImgSrc] = React.useState(logo), imgRef = React.createRef<HTMLImageElement>(),
          [moreDropdownState, setMoreDropdownState] = React.useState({
              show: false,
              className: 'more-dropdown'
          }),
          [showMoreButton, setShowMoreButton] = React.useState(false);

    const makeDetailsProps = () => {
        if (props.image) {
            return {
                name: props.image.name,
                dateCreated: props.image.createdAt,
                dateChanged: props.image.lastChanged,
                mimeType: props.image.mimeType,
                size: `${props.image.width}x${props.image.height}`
            }
        } else if (props.gallery) {
            return {
                name: props.gallery.name,
                dateCreated: props.gallery.createdAt,
                dateChanged: props.gallery.lastChanged,
                mimeType: 'Gallery',
                imageCount: props.gallery.imageCount
            }
        } else {
            return null;
        }
    }

    React.useEffect(() => {
        if (imgRef.current) {
            if (props.image) {
                setImgSrc(Api.makeImageSrc(props.image.id, imgRef.current.clientWidth, imgRef.current.clientHeight));
            }
        }
    }, [props.image, imgRef])

    return (
        <Col xl={2} lg={3} md={4} sm={5} className='px-2 my-2 grid-cell'>
            <div className='grid-cell-div'>
                <div className='grid-image-div'>
                    <Dropdown drop='left' {...moreDropdownState}
                        onToggle={(show: boolean) => setMoreDropdownState({show: show, className: `more-dropdown${show ? ' show' : ''}`})}>
                        <Dropdown.Toggle as={MoreToggle as any} id='more-dropdown-toggle'/>
                        <Dropdown.Menu>
                            <Dropdown.Item onClick={() => props.showDetails(makeDetailsProps())}>Show Details</Dropdown.Item>
                            <Dropdown.Item onClick={() => {
                                if (props.image) {
                                    props.deleteImage(props.image.id);
                                } else if (props.gallery) {
                                    props.deleteGallery(props.gallery.id);
                                }
                            }}>
                                Delete
                            </Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                    <img ref={imgRef} src={imgSrc} className='grid-image'
                        onClick={(e: any) => props.gallery ? props.setGallery(props.gallery) : props.showImage(props.image)}/>
                </div>
                {props.gallery &&
                    <div className='grid-name background-snd'>
                        {props.gallery.name}
                    </div> 
                }
            </div>
        </Col>
    )
}

interface ShowImageProps {
    image: Api.Image,
    close: () => void
}

const ShowImage: React.FC<ShowImageProps> = (props) => {
    const [imgSrc, setImgSrc] = React.useState(''), imgDivRef = React.createRef<HTMLDivElement>(),
          [windowResize, setWindowResize] = React.useState(false),
          imgInfoRef = React.createRef<HTMLDivElement>(),
          rootDiv = React.createRef<HTMLDivElement>();

    React.useEffect(() => {
        if (imgInfoRef.current && imgDivRef.current) {
            let infoWindowPos = window.innerHeight - imgInfoRef.current.clientHeight;
            setImgSrc(Api.makeImageSrc(props.image.id, window.innerWidth, infoWindowPos));
            imgInfoRef.current.style.top = `${document.documentElement.scrollTop + infoWindowPos}px`;
            imgDivRef.current.style.height = `${infoWindowPos}px`;

            setWindowResize(false);

            let subscription = fromEvent(window, 'resize').pipe(
                debounceTime(200),
            ).subscribe(e => setWindowResize(true));

            return () => {
                subscription.unsubscribe();
            }
        }
    }, [props.image, imgDivRef, imgInfoRef, windowResize])

    React.useEffect(() => {
        let old = document.body.style.overflow;
        document.body.style.overflow = 'hidden';

        return () => {
            document.body.style.overflow = old;
        }
    })

    React.useEffect(() => {
        if (rootDiv.current) {
            rootDiv.current.style.top = `${document.documentElement.scrollTop}px`;
        }
    }, [rootDiv])

    return (
        <div ref={rootDiv} className='showimage'>
            <div ref={imgDivRef} className='showimage-image-div'>
                <img src={imgSrc} className='showimage-image'/>
            </div>
            <div ref={imgInfoRef} className='showimage-info'>
                <Button variant='primary' onClick={props.close}>Close</Button>
                <strong>{props.image.name}</strong>
            </div>
        </div>
    )
}

export interface GridViewProps {
    gallery?: Api.Gallery,
    resources: Api.Gallery[] | Api.Image[],
    setGallery: (gallery: Api.Gallery) => void,
    deleteImage: (id: string) => void,
    deleteGallery: (id: string) => void
}

const GridView: React.FC<GridViewProps> = (props) => {
    const [showImage, setShowImage] = React.useState<ShowImageProps | null>(null),
          [detailsProps, setDetailsProps] = React.useState<DetailsVisualProps | null>(null),
          [showDetails, setShowDetails] = React.useState(false);

    const showImageCallback = (image?: Api.Image) => {
        setShowImage(image ? {image: image, close: () => setShowImage(null)} : null);
    }

    const makeCell = (res: Api.Image | Api.Gallery) => {
        let cellProps = {
            showImage: showImageCallback,
            setGallery: props.setGallery,
            showDetails: (details: DetailsVisualProps | null) => {
                if (details) {
                    setDetailsProps(details);
                    setShowDetails(true);
                }
            },
            deleteImage: props.deleteImage,
            deleteGallery: props.deleteGallery
        };

        if (!props.gallery) {
            cellProps = Object.assign(cellProps, {gallery: res as Api.Gallery});
        } else {
            cellProps = Object.assign(cellProps, {image: res as Api.Image});
        }
        return (
            <Cell key={res.id} {...cellProps} />
        )
    }

    const cells = props.gallery ? (props.resources as Api.Image[]).map(makeCell)
                                : (props.resources as Api.Gallery[]).map(makeCell);

    return (
        <>
            <Container className='px-2 my-2' fluid>
                <Row className='mx-n2 py-n2 justify-content-sm-center justify-content-md-start'>
                    {cells}
                </Row>
            </Container>
            {showImage &&
            <ShowImage {...showImage} />
            }
            {detailsProps &&
            <DetailsModal {...Object.assign({}, detailsProps, {show: showDetails, onHide: () => setShowDetails(false)})} />
            }
        </>
    )
}

export default GridView;