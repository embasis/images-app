import React from 'react';
import {of, from, merge, concat, Observable, fromEvent} from 'rxjs';
import {catchError, map, bufferToggle, mergeAll, debounceTime, filter, takeWhile, flatMap, toArray} from 'rxjs/operators';
import {Image, Button, Navbar, Container, Col, Row, Form, ButtonGroup, Alert, NavDropdown} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import * as Api from './Api';
import GridView, {GridViewProps} from './GridView'; 
import ImageUploader from './ImageUploader';
import GalleryCreator from './GalleryCreator';
import UploadIMG from './upload.png';

interface NavElementProps<T> {
    value: T,
    callback: (value: T) => void
}

const Order: React.FC<NavElementProps<Api.SortCriteria>> = (props) => {
    const itemName = (() => {
        switch (props.value) {
            case Api.SortCriteria.Name:
                return 'Name';
            case Api.SortCriteria.CreationDate:
                return 'Creation Date';
            case Api.SortCriteria.ChangeDate:
                return 'Last Changed Date';
            default:
                throw Error('wtf');
        }
    })()
    return (
        <NavDropdown title={itemName} id='dropdown-order'>
            <NavDropdown.Item as='button' onClick={(e: any) => props.callback(Api.SortCriteria.Name)}>Name</NavDropdown.Item>
            <NavDropdown.Item as='button' onClick={(e: any) => props.callback(Api.SortCriteria.CreationDate)}>Creation Date</NavDropdown.Item>
            <NavDropdown.Item as='button' onClick={(e: any) => props.callback(Api.SortCriteria.ChangeDate)}>Last Changed Date</NavDropdown.Item>
        </NavDropdown>
    )
}

const Search: React.FC<NavElementProps<string>> = (props) => {
    return (
        <Form.Control type='text' placeholder='Search' value={props.value} onChange={(e: any) => props.callback(e.target.value)} />
    )
}

enum DateType {
    CreatedAt,
    LastChanged
}

const DateFilterDropdown: React.FC<NavElementProps<DateType>> = (props) => {
    const itemName = (() => {
        switch (props.value) {
            case DateType.CreatedAt:
                return 'Creation Date';
            case DateType.LastChanged:
                return 'Last Changed Date';
            default:
                throw Error('wtf');
        }
    })()
    return (
        <NavDropdown title={itemName} id='dropdown-date'>
            <NavDropdown.Item as='button' onClick={(e: any) => props.callback(DateType.CreatedAt)}>Creation Date</NavDropdown.Item>
            <NavDropdown.Item as='button' onClick={(e: any) => props.callback(DateType.LastChanged)}>Last Changed Date</NavDropdown.Item>
        </NavDropdown>
    )
}

const DateFilterField: React.FC<NavElementProps<string>> = (props) => {
    return (
        <Form.Control type='text' maxLength={10} placeholder='mm/dd/yyyy' value={props.value}
            onChange={(e: any) => {props.callback(e.target.value)}} />
    )
}

interface NavComponentProps {
    searchState: Api.SearchProps,
    startDateString: string,
    endDateString: string,
    curDateType: DateType,
    isUploading: boolean,
    setSearchState: (state: Api.SearchProps) => void,
    setStartDate: (dateString: string) => void,
    setEndDate: (dateString: string) => void,
    setCurDateType: (dateType: DateType) => void,
    showUploadModal: () => void,
    showGalleryCreateModal: () => void,
    gotoGalleryList: () => void,
    logout: () => void
}

const NavComponent: React.FC<NavComponentProps> = ({searchState, setSearchState, startDateString,
        setStartDate, endDateString, setEndDate, curDateType, setCurDateType, ...props}) => {
    const dateFilterField = (isStart: boolean) => {
        const nameString = isStart ? 'start' : 'end';

        const updateDate = (dateString: string) => {
            let date: Date | undefined;
            try {
                date = new Date(dateString);
            } catch (e) {
                date = undefined;
            }

            if (curDateType === DateType.CreatedAt) {
                setSearchState(Object.assign({}, searchState, {[`${nameString}CreatedAtDate`]: date}));
            } else {
                setSearchState(Object.assign({}, searchState, {[`${nameString}LastChangedDate`]: date}));
            }

            isStart ? setStartDate(dateString) : setEndDate(dateString);
        }

        return (
            <DateFilterField value={isStart ? startDateString : endDateString} callback={updateDate} />
        )
    }

    return (
        <Navbar id='navbar-main' expand='md' sticky='top' className='background-snd'>
            <Navbar.Toggle aria-controls='navbar-nav' />
            {searchState.galleryID &&
                <Button type='button' variant='primary'
                    onClick={(e:any) => props.gotoGalleryList()}>
                    Back
                </Button>
            }
            <Navbar.Collapse id='navbar-nav'>
                <Container>
                    <Row className='justify-content-center'>
                        <Form inline onSubmit={(e:any) => {e.preventDefault()}}>
                            <Order value={searchState.order} callback={newOrder => setSearchState(Object.assign({}, searchState, {order: newOrder}))} />
                            <Button variant='primary' type='button' onClick={(e: any) => {
                                setSearchState(Object.assign({}, searchState, {descending: !searchState.descending}))}}>
                                {searchState.descending ? '↓' : '↑'}
                            </Button>
                            <Search value={searchState.searchPrefix} callback={newPrefix =>
                                setSearchState(Object.assign({}, searchState, {searchPrefix: newPrefix}))} />
                        </Form>
                        <Form inline onSubmit={((e:any) => {e.preventDefault()})}>
                            <DateFilterDropdown value={curDateType} callback={setCurDateType} />
                                {dateFilterField(true)}
                                {dateFilterField(false)}
                        </Form>
                    </Row>
                </Container>
            </Navbar.Collapse>
            {(searchState.galleryID || props.isUploading) &&
                <Button type='button' onClick={props.showUploadModal}>
                    <Image src={UploadIMG} className='grid-name'/>
                </Button>
            }
            {!searchState.galleryID &&
                <Button type='button' onClick={props.showGalleryCreateModal}>+</Button>
            }
            <Button variant='primary'
                onClick={(e: any) => Api.logout().pipe(catchError(e => 'error')).subscribe(res => {if (res !== 'error') props.logout()})}>
                Log Out
            </Button>
        </Navbar>
    )
}

type ResourceStream = Observable<Api.Gallery[] | Api.Image[]>
interface Source {
    searchProps: Api.SearchProps,
    resources$: ResourceStream
}

const defaultSearchProps = {
    order: Api.SortCriteria.Name,
    descending: false,
    searchPrefix: ''
}

const MainComponent: React.FC<{logout: () => void}> = (props) => {
    const createSource = (searchProps: Api.SearchProps): Source => {
        let resources$ = (searchProps.galleryID ? Api.images(searchProps) : Api.galleries(searchProps)).pipe(
            flatMap(ids => {
                const SLICE_COUNT = 30;
                const getSlice = () => {
                    function restoreOrder<T>(x: [number,T][]) {
                        let result: T[] = [];
                        x.forEach(([i,v]) => result[i] = v);
                        return result;
                    }

                    if (searchProps.galleryID) {
                        return merge(...ids.splice(0, SLICE_COUNT).map((id,i) => {
                            return Api.image(id).pipe(map(res => [i,res] as [number, Api.Image]));
                        })).pipe(toArray(), map(restoreOrder));
                    } else {
                        return merge(...ids.splice(0, SLICE_COUNT).map((id,i) => {
                            return Api.gallery(id).pipe(map(res => [i,res] as [number, Api.Gallery]));
                        })).pipe(toArray(), map(restoreOrder));
                    }
                }
                return concat(getSlice(), fromEvent(document, 'scroll').pipe(
                    filter(e => document.documentElement.scrollHeight - document.documentElement.scrollTop >=
                        Math.max(document.documentElement.scrollHeight * 0.5, document.documentElement.clientHeight + 100)),
                    flatMap(e => getSlice()),
                    takeWhile(v => v !== [])
                ));
            })
        )

        return {searchProps: searchProps, resources$: resources$};
    }

    const [source, setSource] = React.useState(createSource(defaultSearchProps)),
          [showUploadModal, setShowUploadModal] = React.useState(false),
          [showGalleryCreateModal, setShowGalleryCreateModal] = React.useState(false),
          [currentGallery, setCurrentGallery] = React.useState<Api.Gallery | undefined>(undefined),
          [startDateString, setStartDateString] = React.useState(''),
          [endDateString, setEndDateString] = React.useState(''),
          [curDateType, setCurDateType] = React.useState(DateType.CreatedAt),
          [imageToDelete, setImageToDelete] = React.useState(''),
          [galleryToDelete, setGalleryToDelete] = React.useState(''),
          setGallery = (gallery?: Api.Gallery) => {
              setStartDateString('');
              setEndDateString('');
              setCurrentGallery(gallery);
              setGridViewProps(gridViewProps => Object.assign({}, gridViewProps, {gallery: gallery, resources: []}))
              setSource(createSource(Object.assign({}, defaultSearchProps, {galleryID: gallery ? gallery.id : undefined})));
          },
          setSearchState = (state: Api.SearchProps) => {
              setSource(createSource(state));
          },
          setStartDate = (dateString: string) => {
              const date = new Date(dateString);
              setStartDateString(dateString);
              setSource(source => createSource(Object.assign({}, source.searchProps,
                  curDateType === DateType.CreatedAt ? {startCreatedAtDate: date} : {startLastChangedDate: date})));
          },
          setEndDate = (dateString: string) => {
              const date = new Date(dateString);
              setEndDateString(dateString);
              setSource(source => createSource(Object.assign({}, source.searchProps,
                  curDateType === DateType.CreatedAt ? {endCreatedAtDate: date} : {endLastChangedDate: date})));
          },
          gotoGalleryList = () => {
              setCurrentGallery(undefined);
              setStartDateString('');
              setEndDateString('');
              setGridViewProps(gridViewProps => gridViewProps.gallery ? Object.assign({}, gridViewProps, {gallery: undefined, resources: []})
                                                                      : Object.assign({}, gridViewProps));
              setSource(createSource(defaultSearchProps));
          },
          onGalleryCreate = () => {
              setShowGalleryCreateModal(false);
              setSource(source => createSource(Object.assign({}, source.searchProps)));
          },
          [isUploading, setIsUploading] = React.useState(false),
          [gridViewProps, setGridViewProps] = React.useState<GridViewProps>({
              resources: [], setGallery: setGallery, deleteImage: setImageToDelete, deleteGallery: setGalleryToDelete
          });

    React.useEffect(() => {
        const subscription = source.resources$.subscribe(rs => {
            setGridViewProps(gridViewProps => {
                let slice = gridViewProps.resources.slice();
                if (!source.searchProps.galleryID) {
                    let gallerySlice = slice as Api.Gallery[], galleryChunk = rs as Api.Gallery[];
                    gallerySlice.push(...galleryChunk);
                    return Object.assign({}, gridViewProps, {resources: gallerySlice});
                } else {
                    let imageSlice = slice as Api.Image[], imageChunk = rs as Api.Image[];
                    imageSlice.push(...imageChunk);
                    return Object.assign({}, gridViewProps, {resources: imageSlice});
                }
            })
        })
        return () => {
            subscription.unsubscribe();
            setGridViewProps(gridViewProps => Object.assign({}, gridViewProps, {resources: []}));
        }
    }, [source])

    React.useEffect(() => {
        if (imageToDelete !== '') {
            const subscription = Api.deleteImage(imageToDelete).pipe(
                catchError(err => 'error')
            ).subscribe(result => result === 'success' ? setSource(source => createSource(source.searchProps)) : {});

            return () => {
                subscription.unsubscribe();
            }
        }
    }, [imageToDelete])

    React.useEffect(() => {
        if (galleryToDelete !== '') {
            const subscription = Api.deleteGallery(galleryToDelete).pipe(
                catchError(err => 'error')
            ).subscribe(result => result === 'success' ? setSource(source => createSource(source.searchProps)) : {});

            return () => {
                subscription.unsubscribe();
            }
        }
    }, [galleryToDelete])

    return (
        <>
            <ImageUploader show={showUploadModal} onHide={() => setShowUploadModal(false)} gallery={currentGallery}
                onUpload={(value: boolean) => setIsUploading(value)}/>
            <GalleryCreator show={showGalleryCreateModal} onHide={() => setShowGalleryCreateModal(false)} 
                onGalleryCreate={onGalleryCreate}/>
            <NavComponent searchState={source.searchProps} showUploadModal={() => setShowUploadModal(true)}
                showGalleryCreateModal={() => setShowGalleryCreateModal(true)} setSearchState={setSearchState} startDateString={startDateString}
                setStartDate={setStartDate} endDateString={endDateString} setEndDate={setEndDate}
                curDateType={curDateType} setCurDateType={setCurDateType}
                isUploading={isUploading} gotoGalleryList={gotoGalleryList} logout={props.logout}/>
            <GridView {...gridViewProps}/>
        </>
    )
}

export default MainComponent;